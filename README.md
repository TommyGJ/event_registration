# README

My application has two models: Event and Attendee. The event model has multiple 
parameters including name, location, time, and date. This model requires the 
location with respect to date and time to be unique i.e. there cannot be 
two events taking place at the same location at the same time and date.
Furthermore, each event has many attendees. The attendee model belongs to the 
event model and requires an email. This model is constructed in such a way that 
attendees for each event must be unique i.e. the same email cannot be checked in to 
the same event more than once. 

As far as routing and general site layout goes, my site has a home page that directs
the user to pages to create events and manage events. On the create event page,
the user has the option to create a new event with the required event model params. 
Once a new event is successfully created, the user will be directed to a page where
he or she will have the ability to check-in attendees. On the 
manage events page, the user can view all the events. Moreover, the user has the 
option to cancel and view/add attendees for any event on that page. If the user 
decides to cancel an event, the event and its dependent attendees will be removed
from the database. If the user decides to view/add attenddes for an event, a new 
session for that event will be created where he or she can view the attendees and 
their check-in times or add an attendee that will be checked in at the current time. 
Navigation away from an events page will end the session for that event.

