class AttendeesController < ApplicationController
	def create
		@attendee = current_event.attendees.build(attendee_params)
		if @attendee.save
			flash[:success] = "A new attendee was added"
		else
			flash[:danger] = "Could not add attendee because of format/duplicate/blank" 
		end
		redirect_to current_event
	end

	private 
		
		def attendee_params
			params.require(:attendee).permit(:email)
		end
end
