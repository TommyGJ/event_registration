class EventsController < ApplicationController

	def new
    @event = Event.new
  end

  def show
		if managing?
			stop_manage
		end
		@event = Event.find(params[:id])
		manage @event
		@attendees = @event.attendees
		@attendee = @event.attendees.build
		
  end

  def create
    @event = Event.new(event_params)
    if @event.save
			manage @event
			flash[:success] = "You have successfully created an event!"
      redirect_to @event
    else
      render 'new'
    end
  end
	
	def index
		if managing?
			stop_manage
		end
		@events = Event.all
	end

	def destroy
		if managing?
			stop_manage
		end
		Event.find(params[:id]).destroy
		flash[:success] = "Event Canceled!"
		redirect_to manage_event_path
	end

  private

    def event_params
      params.require(:event).permit(:name, :location, :time, :date)
    end
end
