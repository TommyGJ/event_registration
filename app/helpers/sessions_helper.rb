module SessionsHelper
	
  def manage(event)
    session[:event_id] = event.id
  end

  def current_event
    if session[:event_id]
      @current_event ||= Event.find_by(id: session[:event_id])
    end
  end

  def managing?
    !current_event.nil?
  end

  def stop_manage
    session.delete(:event_id)
    @current_event = nil
  end

end
