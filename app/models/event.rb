class Event < ApplicationRecord
	has_many :attendees, dependent: :destroy
	validates :name, presence: true, length: { maximum: 50 }
	validates :location, presence: true, length: { maximum: 100 }
	validates :time, presence: true
	validates :date, presence: true
	validates :location, uniqueness: { scope: [:time, :date] }

end
