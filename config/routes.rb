Rails.application.routes.draw do
  get 'sessions/new'

  get 'events/new'

	root 'static_page#home'
	get '/create_event', to: 'events#new'
	post '/create_event', to: 'events#create'
	get '/manage_event', to: 'events#index'
	resources :events
	resources :attendees, only: [:create]
	

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
