class AddIndexToEvents < ActiveRecord::Migration[5.1]
  def change
		add_index :events, [:location, :time, :date], unique: true
  end
end
