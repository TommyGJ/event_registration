class CreateAttendees < ActiveRecord::Migration[5.1]
  def change
    create_table :attendees do |t|
      t.string :email
      t.references :event, foreign_key: {on_delete: :cascade}

      t.timestamps
    end
  end
end
