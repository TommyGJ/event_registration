require "rails_helper"


#RSpec.configure do |c|
#  c.extend Features
#end
	
feature "User can add/see attendees" do
	scenario "User can add an attendee to his or her event" do
		create_event
		visit manage_event_path
#		page.has_selector?('h2', text: 'Manage Events', visible: true)
		click_link "Add/See Attendees"
		#rails db:migrate for above to work
		fill_in "Enter your email to check in ", with: "tommy.johnson@yale.edu"
		click_button("Check In")
		expect(page).to have_css "div", text: "A new attendee was added"
	end

	scenario "User can see added attendees" do
		create_event
		visit manage_event_path
		click_link "Add/See Attendees"
		fill_in "Enter your email to check in ", with: "tommy.smith@yale.edu"
		click_button("Check In")
		fill_in "Enter your email to check in ", with: "tommy.johnson@yale.edu"
		click_button("Check In")
		fill_in "Enter your email to check in ", with: "tommy.jones@yale.edu"
		click_button("Check In")
		expect(page).to have_css "h3", text: "Attendees (3)"
		expect(page).to have_css "ul.attendees li", count: 3
	end

	scenario "User cannot add already added attendee" do
		create_event
		visit manage_event_path
		click_link "Add/See Attendees"
		fill_in "Enter your email to check in ", with: "tommy.johnson@yale.edu"
		click_button("Check In")
		fill_in "Enter your email to check in ", with: "tommy.johnson@yale.edu"
		click_button("Check In")
		expect(page).not_to have_css "div", text: "A new attendee was added"
	end

	scenario "User can view check-in time of attendee" do
		create_event
		visit manage_event_path
		click_link "Add/See Attendees"
		fill_in "Enter your email to check in ", with: "tommy.johnson@yale.edu"
		click_button("Check In")
		expect(page).to have_css "ul.attendees li time"
	end
end



