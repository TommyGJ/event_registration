require "rails_helper"


RSpec.configure do |c|
  c.extend Features
end
	
feature "User can cancel events" do
	scenario "User cancels an event" do
		create_event
		visit manage_event_path
#		page.has_selector?('h2', text: 'Manage Events', visible: true)
		expect(page).to have_css "h1", text: "Manage Events"
		click_link "Cancel Event"
		#rails db:migrate for above to work
		expect(page).to have_css "div", text: "Event Canceled!"
	end
end



