require "rails_helper"

feature "User creates new event" do
	scenario "User gives invalid event information" do
		visit create_event_path
		fill_in "Name", with: ""
		fill_in "Location", with: ""
		fill_in "Date", with: ""
		fill_in "Time", with: ""
		click_button("Create Event")
		expect(page).to have_css "div", text: "The form contains 4 errors."
	end

	scenario "User gives valid event information" do
		visit create_event_path
		fill_in "Name", with: "Meeting"
		fill_in "Location", with: "Front Office"
		fill_in "Date", with: DateTime.new(2019,11,5)
		fill_in "Time", with: Time.new(2019,11,5,8,30)
		click_button("Create Event")
		expect(page).to have_css "div", text: "You have successfully created an event!" 
		
	end
end
