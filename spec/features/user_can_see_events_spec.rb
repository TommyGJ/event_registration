require "rails_helper"


RSpec.configure do |c|
  c.extend Features
end
	
feature "User can cancel events" do
	scenario "User cancels an event" do
		create_event
		visit manage_event_path
#		page.has_selector?('h2', text: 'Manage Events', visible: true)
		expect(page).to have_css "h1", text: "Manage Events"
		expect(page).to have_css "ul.events li", count: 1
	end
end



