require "rails_helper"

describe Attendee, "Validate" do
	it "should be valid" do
		d = DateTime.new(2019,11,12)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		attendee = event.attendees.build(email: "tommy.johnson@yale.edu")
		expect(attendee).to be_valid
	end

	it "should be invalid because of nil email" do 
		d = DateTime.new(2019,11,12)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		attendee = event.attendees.build(email: "")
		expect(attendee).not_to be_valid
	end

	it "should be invalid because of wrong format"  do
		d = DateTime.new(2019,11,12)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		attendee = event.attendees.build(email:"tommy.johnson") 
		expect(attendee).not_to be_valid
	end

	it "should be invalid because email is too long" do
		d = DateTime.new(2019,11,12)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		attendee = event.attendees.build(email: "a" * 255 + "@example.com")
		expect(attendee).not_to be_valid
	end
	
	it "should be invalid because email is a duplicate" do
		d = DateTime.new(2019,11,12)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		event.attendees.create!(email: "tommy.johnson@yale.edu")
		attendee = event.attendees.build(email: "tommy.johnson@yale.edu")
		expect(attendee).not_to be_valid
	end

	it "should be valid when checking into two different events" do
		d = DateTime.new(2019,11,12)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		event2 = Event.create!(name: "Team Meeting", location: "Home", date: d, time: t + 3600)
		event.attendees.create!(email: "tommy.johnson@yale.edu")
		attendee = event2.attendees.build(email: "tommy.johnson@yale.edu")
		expect(attendee).to be_valid
	end




	
end
