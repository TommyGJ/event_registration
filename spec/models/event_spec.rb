require "rails_helper"

describe Event, "Validation" do
	it "should be valid" do
		d = DateTime.new(2019,11,12)
		t = Time.now
		event = Event.new(name: "Team Meeting", location: "Office", date: d, time: t)
		expect(event).to be_valid
	end
	it "should not be valid" do
		event = Event.new(name: nil, location: nil , date: nil, time: nil)
		expect(event).not_to be_valid
	end
	it "should not be valid because of same location and time" do
		d = DateTime.new(2019,11,10)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		expect(event).to be_valid
		event2 = Event.create(name: "Poker", location: "Office", date: d, time: t)
#		event2 = Event.new(name: "Poker ", location: "Office", date: d, time: t)
		expect(event2).not_to be_valid
	end

	it "should be valid with same time but different location" do
		d = DateTime.new(2019,11,10)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		expect(event).to be_valid
		event2 = Event.create(name: "Poker", location: "School", date: d, time: t)
#		event2 = Event.new(name: "Poker ", location: "Office", date: d, time: t)
		expect(event2).to be_valid
	end

	it "should be valid with same location but different times" do
		d = DateTime.new(2019,11,10)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		expect(event).to be_valid
		t += 3600
		event2 = Event.create(name: "Team Meeting", location: "School", date: d, time: t)
#		event2 = Event.new(name: "Poker ", location: "Office", date: d, time: t)
		expect(event2).to be_valid
	end
end

describe Event, "Associated Attendees" do
	it "should be destroyed upon event destruction" do
		d = DateTime.new(2019,11,10)
		t = Time.now
		event = Event.create!(name: "Team Meeting", location: "Office", date: d, time: t)
		event.attendees.create!(email: "tommy.johnson@yale.edu")
		event.destroy
		expect(Attendee.count).to eq(0)
	end
end 

