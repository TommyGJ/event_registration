module Features
	def create_event
		visit create_event_path
		fill_in "Name", with: "Meeting"
		fill_in "Location", with: "Front Office"
		fill_in "Date", with: DateTime.new(2019,11,5)
		fill_in "Time", with: Time.new(2019,11,5,8,30)
		click_button("Create Event")
	end

end
